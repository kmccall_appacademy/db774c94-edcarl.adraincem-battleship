require_relative 'board.rb'
require_relative 'player.rb'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(pos)
    board[pos] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    pos = player.get_play
    attack(pos)
  end

end

if __FILE__ == $PROGRAM_NAME
  board = Board.new
  player = HumanPlayer.new("test")
  game = BattleshipGame.new(player, board)
  game.play_turn
end
