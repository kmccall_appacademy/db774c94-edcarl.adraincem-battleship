class Board
  ELEMENTS = {
    :s => "s",
    :x => "x",
    nil => " "
  }

  attr_accessor :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    grid[row][col] = val
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def count
    grid.flatten.count(:s)
  end

  def empty?(pos = nil)
    if pos.nil?
      count == 0
    else
      self[pos].nil?
    end
  end

  def full?
    grid.flatten.all? { |el| el == :s }
  end

  def place_random_ship
    if full?
      raise "ERROR - board is full"
    else
      row = rand(grid.length)
      col = rand(grid[0].length)
      pos = row, col
      self[pos] = :s if empty?(pos)
    end
  end

  def won?
    grid.flatten.none? { |el| el == :s }
  end

end
