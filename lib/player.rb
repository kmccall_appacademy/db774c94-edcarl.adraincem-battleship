class HumanPlayer

  def initialize(name)
    @name = name
  end

  def get_play
    print "Your move to attack (ie. 1,2): "
    input = gets.chomp
    input.split(",").map { |el| el.to_i }
  end

end
